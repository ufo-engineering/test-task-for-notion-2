import React from 'react';
import './App.css';
import { Button } from './components/Button'

function App() {
  return (
    <div className={`App`}>
        <Button
            type={'button'}
            buttonStyle={'btnPrimarySolid'}
            buttonSize={'btnMedium'}
            disabled={true}
        >Test Button 1 (disabled)</Button>
        <Button
            type={'button'}
            buttonStyle={'btnWarningSolid'}
            buttonSize={'btnLarge'}
        >Test Button 2</Button>
        <Button
            type={'submit'}
            buttonStyle={'btnSuccessSolid'}
            buttonSize={'btnMedium'}
        >Test Button 3</Button>
        <Button
            type={'button'}
            buttonStyle={'btnPrimaryOutline'}
            buttonSize={'btnLarge'}
        >Test Button 4</Button>
        <Button
            type={'button'}
            buttonStyle={'btnWarningOutline'}
            buttonSize={'btnLarge'}
            disabled={false}
        >Test Button 5</Button>
        <Button
            type={'button'}
            buttonStyle={'btnSuccessOutline'}
            buttonSize={'btnMedium'}
        >Test Button 6</Button>
    </div>
  );
}

export default App;
