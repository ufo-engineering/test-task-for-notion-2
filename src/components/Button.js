import React, { useState } from 'react';
import styles from './button.module.css';

const STYLES = [
    "btnPrimarySolid",
    "btnWarningSolid",
    "btnSuccessSolid",
    "btnPrimaryOutline",
    "btnWarningOutline",
    "btnSuccessOutline"
]

const SIZES = [
    "btnMedium",
    "btnLarge"
]

export const Button = ({children, type, onClick, buttonStyle, buttonSize, disabled}) => {

   const [isActive, setIsActive] = useState(false);

   const checkButtonStyle = STYLES.includes(buttonStyle)
       ? buttonStyle
       : STYLES[0];

    const checkButtonSize = SIZES.includes(buttonSize)
        ? buttonSize
        : SIZES[0];

    const isButtonDisabled = disabled === undefined
        ? false
        : disabled

    const checkButtonActivity = isActive
         ? 'btnActive'
         : ''

    return(
        <button
            type={type}
            onClick={() => setIsActive(!isActive)}
            className={`${styles.btn} ${styles[checkButtonStyle]} ${styles[checkButtonSize]} ${styles[checkButtonActivity]}`}
            disabled={isButtonDisabled}
        >
            {children}
        </button>
    )
}
